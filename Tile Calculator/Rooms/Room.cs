﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tile_Calculator.Materials;

namespace Tile_Calculator.Rooms
{
    public abstract class Room
    {
        public List<Segment> Segments { get; set; }
        public double SquareFeet
        {
            get
            {
                double sqft = 0;

                Segments.ForEach(seg =>
                {
                    sqft += seg.Area;
                });

                return Math.Round(sqft, 3);
            }
        }
        public double TilesRequired { get; set; }
        public double TilesCut { get; set; }
        public double TilesReused { get; set; }
        public double TileWastePct
        {
            get
            {
                double trash = TilesCut - TilesReused;
                return Math.Round((trash / TilesRequired), 4) * 100;
            }
        }

        public abstract void EditRoomSegments();

        public Room()
        {
            Segments = new List<Segment>();
        }

        public double AddGrout(int length, GroutItem grout)
        {
            // half the grout for each tile
            return length + grout.Size;
        }

        public void CalculateCutsCheckerboard(double length, double width)
        {

        }

        public void CalculateCutsBrickwork()
        {

        }

        public void CalculateCuts(double total, bool alternating = false) 
        {
            double cut = total / 2;

            if (alternating)
            {
                cut /= 2;
            }

            double reused = Math.Floor(cut);
                       
            TilesRequired -= reused;
            TilesReused += reused;
            TilesCut += reused;

            if ((cut - reused > 0))
            {
                TilesCut += 1;
            }
        }

        public void CalculateMaterial(TileItem tile, TilePatterns pattern, GroutItem grout)
        {
            // clear props
            TilesRequired = 0;
            TilesCut = 0;
            TilesReused = 0;

            foreach (var seg in Segments)
            {
                // calculate total run after adding grout to both ends
                double tilesLength = (seg.Length - grout.Size) / AddGrout(tile.Length, grout);
                double tilesWidth = (seg.Width - grout.Size) / AddGrout(tile.Width, grout);

                // calculate waste per run
                double lengthWaste = tilesLength - Math.Floor(tilesLength);
                double widthWaste = tilesWidth - Math.Floor(tilesWidth);

                // can we make a single cut and reuse the endpieces?
                bool isLengthWasteReusable = lengthWaste == 0.5;
                bool isWidthWasteReusable = widthWaste == 0.5;
                bool isLengthWasteAlternating = false;
                bool isWidthWasteAlternating = false;

                if (pattern == TilePatterns.Brickwork)
                {
                    isLengthWasteAlternating = lengthWaste == 0 || lengthWaste == 0.25;
                    isWidthWasteAlternating = widthWaste == 0 || widthWaste == 0.25;
                }

                // calculate total tiles
                double totalTileLength = Math.Ceiling(tilesLength);
                double totalTileWidth = Math.Ceiling(tilesWidth);

                TilesRequired += totalTileLength * totalTileWidth;

                // calculate reused and trashed
                if (isLengthWasteReusable || isLengthWasteAlternating)
                {
                    CalculateCuts(totalTileWidth, isLengthWasteAlternating);                    
                }
                else if (!isLengthWasteReusable && !isLengthWasteAlternating && lengthWaste > 0)
                {
                    TilesCut += totalTileWidth;
                }

                if (isWidthWasteReusable || isWidthWasteAlternating)
                {
                    CalculateCuts(totalTileLength, isWidthWasteAlternating);
                }
                else if (!isWidthWasteReusable && !isWidthWasteAlternating && widthWaste > 0)
                {
                    TilesCut += totalTileLength;
                }

                // account for the corner piece
                if (isLengthWasteReusable && isWidthWasteReusable)
                {
                    TilesCut -= 1;
                }
            }
        }
    }

    public enum RoomType
    {
        Rectangle,
        Ell,
        Tee
    }
}
