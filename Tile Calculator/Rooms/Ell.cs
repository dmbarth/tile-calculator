﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tile_Calculator.Materials;

namespace Tile_Calculator.Rooms
{
    public class Ell : Room
    {
        public override void EditRoomSegments()
        {
            Forms.Rooms.Ell frm = new Forms.Rooms.Ell();

            if (Segments.Count > 0)
            {
                frm.SegALength = Segments[0].Length;
                frm.SegAWidth = Segments[0].Width;

                frm.SegBLength = Segments[1].Length;
                frm.SegBWidth = Segments[1].Width;
            }

            frm.ShowDialog();

            if (frm.DialogResult == DialogResult.OK)
            {
                Segments.Clear();

                Segments.Add(new Segment(frm.SegALength, frm.SegAWidth));
                Segments.Add(new Segment(frm.SegBLength, frm.SegBWidth));
            }
        }
    }
}
