﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tile_Calculator.Materials;


namespace Tile_Calculator.Rooms
{
    public class Rectangle : Room
    {
        public override void EditRoomSegments()
        {
            Forms.Rooms.Rectangle frm = new Forms.Rooms.Rectangle();

            if (Segments.Count > 0)
            {
                frm.SegALength = Segments[0].Length;
                frm.SegAWidth = Segments[0].Width;
            }

            frm.ShowDialog();

            if (frm.DialogResult == DialogResult.OK)
            {
                Segments.Clear();

                Segments.Add(new Segment(frm.SegALength, frm.SegAWidth));
            }
        }
    }
}
