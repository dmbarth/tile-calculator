﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tile_Calculator.Rooms
{
    public class Segment
    {
        public string Designation { get; set; }
        public double Length { get; set; }
        public double Width { get; set; }
        public double Area
        {
            get
            {
                // convert back to ft
                return (Length / 12) * (Width / 12);
            }
        }

        public Segment(double length, double width)
        {
            Length = length;
            Width = width;
        }
    }
}
