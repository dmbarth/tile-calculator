﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tile_Calculator.Materials
{
    public static class Tile
    {
        public static List<TileItem> Items()
        {
            List<TileItem> items = new List<TileItem>();

            items.Add( new TileItem(1, 2));
            items.Add( new TileItem(1, 4));
            items.Add( new TileItem(1, 6));
            items.Add( new TileItem(1, 8));
            items.Add( new TileItem(2));
            items.Add( new TileItem(2, 4));
            items.Add( new TileItem(2, 6));
            items.Add( new TileItem(2, 8));
            items.Add( new TileItem(4));
            items.Add( new TileItem(4, 6));
            items.Add( new TileItem(4, 8));
            items.Add( new TileItem(4, 10));
            items.Add( new TileItem(6));
            items.Add( new TileItem(6, 8));
            items.Add( new TileItem(6, 10));
            items.Add( new TileItem(6, 12));
            items.Add( new TileItem(8));
            items.Add( new TileItem(8, 10));
            items.Add( new TileItem(8, 12));
            items.Add( new TileItem(10));
            items.Add( new TileItem(10, 12));
            items.Add( new TileItem(10, 20));
            items.Add( new TileItem(12));
            items.Add( new TileItem(12, 24));
            items.Add( new TileItem(16));
            items.Add( new TileItem(16, 24));
            items.Add( new TileItem(17));
            items.Add( new TileItem(18));
            items.Add( new TileItem(20));
            items.Add( new TileItem(24));

            return items;
        }
    }

    public class TileItem
    {
        public int Length { get; set; }
        public int Width { get; set; }
        public string Text
        {
            get
            {
                return Length + "x" + Width;
            }
        }

        public TileItem(int length)
        {
            Length = length;
            Width = length;
        }

        public TileItem(int length, int width)
        {
            Length = length;
            Width = width;
        }
    }

    public enum TilePatterns
    {
        Brickwork,
        Checkerboard
    }
}
