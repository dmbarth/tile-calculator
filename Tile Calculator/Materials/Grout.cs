﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tile_Calculator.Materials
{
    public static class Grout
    {
        public static List<GroutItem> Items()
        {
            List<GroutItem> items = new List<GroutItem>();

            items.Add( new GroutItem("1/8"));
            items.Add( new GroutItem("1/4"));
            items.Add( new GroutItem("3/8"));
            items.Add( new GroutItem("1/2"));

            return items;
        }
    }

    public class GroutItem
    {
        public string Fraction { get; set; }
        public double Size 
        {
            get
            {
                return Helpers.Fractions.ToDouble(Fraction);
            }
        }

        public GroutItem() { }
        public GroutItem(string textValue)
        {
            Fraction = textValue;
        }
    }
}
