﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tile_Calculator.UserControls
{
    public partial class TextInput : UserControl
    {
        [
        Category("Custom"),
        Description("Get and Set the Text value")
        ]
        public virtual string TextValue {
            get 
            {
                return txtValue.Text;
            }
            set
            {
                txtValue.Text = value;
            }
        }

        public TextInput()
        {
            InitializeComponent();
        }

        private void TextInput_Resize(object sender, EventArgs e)
        {
            // Center the textbox - the plus 1 helps with rounding
            txtValue.Top = (this.Height - txtValue.Height) / 2 + 1;
        }        
    }
}
