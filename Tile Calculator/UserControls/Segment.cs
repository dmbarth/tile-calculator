﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tile_Calculator.UserControls
{
    public partial class Segment : UserControl
    {
        private enum Units
        {
            inches,
            feet
        }

        private string _designation;
        private double _length;
        private double _width;
        private string _errorMsg = "Values must be greater than zero.";

        [Category("Custom")]
        public string Designation 
        {
            get { return _designation; }
            set
            {
                _designation = value;

                grpSegment.Text = "Segment " + value;
            }
        }

        [Category("Custom")]
        public double SWidth
        {
            get 
            {
                if ((Units)cmbWidth.SelectedItem == Units.feet)
                {
                    return _width * 12;
                }

                return _width; 
            }
            set
            {
                _width = value;

                txtWidth.Text = _width.ToString();
            }
        }

        [Category("Custom")]
        public double SLength
        {
            get 
            {
                if ((Units)cmbLength.SelectedItem == Units.feet)
                {
                    return _length * 12;
                }

                return _length; 
            }
            set
            {
                _length = value;

                txtLength.Text = _length.ToString();
            }
        }

        public Segment()
        {
            InitializeComponent();

            cmbLength.DataSource = Enum.GetValues(typeof(Units));
            cmbWidth.DataSource = Enum.GetValues(typeof(Units));
        }

        public bool IsLengthValid()
        {
            if (_length > 0) return true;

            this.errorProvider1.SetError(txtLength, _errorMsg);

            return false;
        }

        public bool IsWidthValid()
        {
            if (_width > 0) return true;

            this.errorProvider2.SetError(txtWidth, _errorMsg);

            return false;
        }

        private void validate_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }
        
        // Events
        
        private void txtLength_TextChanged(object sender, EventArgs e)
        {
            double value;

            double.TryParse((sender as TextBox).Text, out value);

            if (value != _length)
            {
                _length = value;
            }

            if (_length > 0) this.errorProvider1.Clear();
        }

        private void txtWidth_TextChanged(object sender, EventArgs e)
        {
            double value;

            double.TryParse((sender as TextBox).Text, out value);

            if (value != _width)
            {
                _width = value;
            }

            if (_width > 0) this.errorProvider2.Clear();
        }
    }
}
