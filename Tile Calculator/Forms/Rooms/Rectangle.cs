﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tile_Calculator.Forms.Rooms
{
    public partial class Rectangle : Form
    {
        public double SegALength 
        {
            get { return segmentA.SLength; }
            set { segmentA.SLength = value; }
        }
        public double SegAWidth
        {
            get { return segmentA.SWidth; }
            set { segmentA.SWidth = value; }
        }

        public Rectangle()
        {
            InitializeComponent();
        }

        // Events

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            bool segALength = segmentA.IsLengthValid();
            bool segAWidth = segmentA.IsWidthValid();

            if (segALength && segAWidth)
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }
    }
}
