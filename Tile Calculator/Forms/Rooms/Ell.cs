﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tile_Calculator.Forms.Rooms
{
    public partial class Ell : Form
    {
        public double SegALength
        {
            get { return segmentA.SLength; }
            set { segmentA.SLength = value; }
        }
        public double SegAWidth
        {
            get { return segmentA.SWidth; }
            set { segmentA.SWidth = value; }
        }
        public double SegBLength
        {
            get { return segmentB.SLength; }
            set { segmentB.SLength = value; }
        }
        public double SegBWidth
        {
            get { return segmentB.SWidth; }
            set { segmentB.SWidth = value; }
        }

        public Ell()
        {
            InitializeComponent();
        }

        // Events
        
        private void btnSubmit_Click(object sender, EventArgs e)
        {
            bool segALength = segmentA.IsLengthValid();
            bool segAWidth = segmentA.IsWidthValid();

            bool segBLength = segmentB.IsLengthValid();
            bool segBWidth = segmentB.IsWidthValid();

            if (segALength && segAWidth && segBLength && segBWidth)
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }
    }
}
