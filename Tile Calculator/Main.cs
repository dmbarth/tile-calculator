﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Tile_Calculator.Rooms;
using Tile_Calculator.Materials;

namespace Tile_Calculator
{
    public partial class Main : Form
    {
        private Room _room;
        private bool _isLoaded;

        public Main()
        {
            InitializeComponent();
            
            // initialize data
            cmbRoomType.DataSource = Enum.GetValues(typeof(RoomType));

            cmbTileSize.DataSource = Tile.Items();
            cmbTileSize.DisplayMember = "Text";

            cmbTilePattern.DataSource = Enum.GetValues(typeof(TilePatterns));

            cmbGrout.DataSource = Grout.Items();
            cmbGrout.DisplayMember = "Fraction";
        }

        private void EditRoomSegments(RoomType room)
        {
            if (room == RoomType.Rectangle)
            {
                _room = new Rooms.Rectangle();
                _room.EditRoomSegments();
            }

            if (room == RoomType.Ell)
            {
                _room = new Rooms.Ell();
                _room.EditRoomSegments();
            }

            if (room == RoomType.Tee)
            {
                _room = new Rooms.Tee();
                _room.EditRoomSegments();
            }
        }

        private bool IsRoomValid()
        {
            if (_room.SquareFeet == 0)
            {
                string errorMsg = "SqFt cannot be zero.";
                this.errorProvider1.SetError(lblSquareFeet, errorMsg);

                return false;
            }

            return true;
        }

        // Events

        private void Main_Load(object sender, EventArgs e)
        {
            _isLoaded = true;

            // set default room
            _room = new Rooms.Rectangle();
        }

        private void btnEditRoom_Click(object sender, EventArgs e)
        {
            _room.EditRoomSegments();
            lblSquareFeet.Text = _room.SquareFeet.ToString();
        }

        private void cmbRoomType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!_isLoaded) return;

            RoomType room = (RoomType)cmbRoomType.SelectedItem;

            EditRoomSegments(room);
        }

        private void btnCalculate_Click(object sender, EventArgs e)
        {
            if (!IsRoomValid()) return;

            TileItem tile = (TileItem)cmbTileSize.SelectedItem;
            TilePatterns pattern = (TilePatterns)cmbTilePattern.SelectedItem;
            GroutItem grout = (GroutItem)cmbGrout.SelectedItem;

            _room.CalculateMaterial(tile, pattern, grout);

            txtTileRequired.Text = _room.TilesRequired.ToString();
            txtTileCut.Text = _room.TilesCut.ToString();
            txtTileReused.Text = _room.TilesReused.ToString();
            txtTileWastePct.Text = _room.TileWastePct.ToString();     
        }

        private void lblSquareFeet_TextChanged(object sender, EventArgs e)
        {
            if (_room.SquareFeet > 0)
            {
                this.errorProvider1.Clear();
            }
        }
    }
}
